/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciolp1;

import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class ExercicioLP1 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int n;
        
        do {
            System.out.println("Introduza o valor de N: ");
            n = scanner.nextInt();
        } while (n < 1 || n > 100);
        
        System.out.println("O resultado da soma é de: " + soma(n));
    }
    
    static int soma (int n) {
    	int retValue = 0; 
 
        for (int i = 1; i <= n; i++) {
            if ((i % 3) == 0) {
                retValue = retValue + i;
            }
        }
        
    	return retValue;
    }
}
